
Pod::Spec.new do |s|

  s.name         = "RaxelPulse"
  s.version      = "5.11-rc40"
  s.summary      = "RaxelPulseFramework"

  s.description  = "Framework for telematics data"

  s.homepage     = "https://docs.telematicssdk.com"

  s.license      = { :type => "Proprietary", :text => "https://www.raxeltelematics.com/ 2020 © Raxel Telematics. All rights reserved." }

  s.author             = { "Sergey Emelyanov" => "se@raxeltelematics.com" }

  s.ios.deployment_target = "9.0"
  s.ios.vendored_frameworks = 'RaxelPulse.framework'

  s.source       = { :http => "https://iosstorage1.blob.core.windows.net/pulse-ios/RaxelPulse-" + s.version.to_s + ".zip" }
  
  s.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end
